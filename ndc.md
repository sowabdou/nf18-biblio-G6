# NF18 - Note de clarification

## Déclaration des objets

### Ressource

Contient code, un titre, une date d'apparition, un éditeur, un genre et un code de classification.
Elle peut avoir plusieurs type avec des informations en plus :

- livres
    - **contributeurs** = auteurs
    - On doit en plus sauvegarder les infos: ISBN d'un livre ,résumé , la langue des documents écrits

- film
    - **contributeurs** = réalisateurs ou acteurs
    - On doit en plus sauvegarder les infos: la longueur,le synopsis

- enregistrement musicaux
    - **contributeurs** = compositeurs ou interprètes.
    - On doit en plus sauvegarder les infos: la longueur.

#### Associations

Une ressource peut avoir plusieurs contributeurs (1..* - 1..*).

#### Contraintes

- Le code doit être unique

---

### Exemplaire

Un exemplaire est lié à une ressource et il peut exister plusieurs exemplaires pour une même ressource.

#### Contraintes

Sans ressource, un exemplaire ne peut exister.
La Ressource doit exister avant l'exemplaire.
L'exemplaire est une composition de la ressource.

---

### Contributeur

* Contient les informations : nom, son prénom, sa date de naissance et sa nationalité.

#### Associations

Un contributeur peut avoir contribué à plusieurs ressources différentes.(1..* - 1..*)

---

### Compte utilisateur

* le compte utilisateur est constitué d'un login et d'un mot de passe

#### Contraintes

Sans membre, un compte utilisateur ne peut exister.
Composition du membre.

---

### *Membre*

* Un membre est caractérisé par son nom,son prenom,sa date de naissance son adresse et son e-mail.
* Chaque membre est lié à un compte utilisateur.

Un membre est une classe abstraite qui peut permet de créer un Membre du personnel ou un Adhérent.

---

### Adhérent

* Un adhérent est un membre ayant un numéro de téléphone et une carte d'adhérent qui lui permet d'emprunter des
  documents

#### Associations

Un adhérent peut emprunter plusieurs exemplaires à l'aide de prêts Adherent (* - *) Prêts.

---

### Membre du personnel

* chaque membre du personnel est caractérisé par son nom, son prénom, son adresse et son adresse e-mail.
* un membre du personnel dispose d'un compte utilisateur;
* Un membre du personnel peut être administrateur ou non.

#### Remarques

Un menbre du personnel peut creer, modifier, supprimer des ressources, des exemplaires, des contributeurs, des prets,
des emprunts, des retards, des réservations, des membres du personnel, des adhérents.

---

### Prêt

* Chaque prêt est caractérisé par l'adhérent ayant fait le prêt, la ressource empruntée, la une date de prêt et une
  durée
  de prêt. Un adhérent peut être sanctionné s'il ne respecte pas la date de retour du prêt.

#### Associations

Un pret est lié à un adhérent et un Exemplaire :

- Adherent (* - *) Prêts.
- Exemplaire (1 - *) Prêts.

---

### Sanction

* Une sanction est lié à un prêt spécifique;elle est representé par un type de sanction (retard ou deterioration).
    - Un retard est constitué d'une date de retour, d'une durée de retard (et donc de suspension)
    - Une deterioration est constitué de l'état de remboursement du document (remboursé ou pas)

#### Contraintes

Il peut y avoir deux sanctions pour un prêt
Une sanction est une composition d'un prêt car sans prêt il ne peut pas il y avoir de sanctions.

## Demandes d'informations sur la base de données
- Pour savoir si un exemplaire est emprunté, il faut vérifier s'il n'a pas de prêts associés avec le statut `En cours`
- Pour savoir si l'adhérent a une suspension de prêts, il faut calculer si la durée du retard est superieur à la date actuelle moins la date de rendu du livre.
- Pour savoir si un membre est un membre du personnel il faut vérifier s'il existe dans membre du personnel
- Pour connaitre le nombre d'exemplaires en prêts il faut compter tous les prêts avec l'état `En cours`

## Gestion et droits utilisateurs
Les deux genres d'utilisateurs sont.
### Adhérent
  Il a le droit de lire les ressources, ses prêts.
### Membre du personnel
Un membre du personnel peut en général tout faire sauf supprimer des prêts Terminées, sanctions et accéder aux Membres du personnel.
- Si le membre du personnel est administrateur, il peut créer, modifier, supprimer des Membres du personnel.