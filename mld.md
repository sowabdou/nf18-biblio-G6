# Modele logique de données

## Partie 1:

C'est un héritage par référence avec la classe mère abstraite. Il faut ajouter la contrainte que tous les tuples de R1
sont référencés par un tuple de R2 ou de R3.

* **Membre**(#id_membre:INTEGER , nom:VARCHAR(10), prénom:VARCHAR(10), adresse:VARCHAR(15), email:VARCHAR(15) )

* **Adhérent**(#id_membre => Membre , date_de_naissance :DATE , numéro_de_téléphone :VARCHAR(15) , carte_adhérent:
  VARCHAR(
    20) ) avec carte_adhérent UNIQUE


* **MembreDuPersonnel**(#id_membre => Membre  )


* **CompteUtilisateur**(#login: VARCHAR(10), mdp: VRARCHAR(10), id_membre => Membre  ) avec id-membre UNIQUE

## Partie 2

Un pret peut etre fait seulement après la vérification que l'adhérent n'a pas plus que 5 sanctions.

* **Pret**(#id_pret: INTEGER , date_debut :DATE , etat:{en cours , etat_fini} , durée: INTEGER , exemplaire =>
  Exemplaire.id,
  adherent =>Adherent.id_membre) avec adhérent NOT NULL et exemplaire NOT NULL

La contraint pour la relation entre "prêt" et "Adhérent" est "adhérent NOT NULL" (le cardinalité est 1-0..N)

La contraint pour la relation entre "prêt" et "Exemplaire" est "exemplaire NOT NULL" (le cardinalité est 1-0..N)

* **Retard**(#id_pret => Pret.id_pret , date_retour: DATE , Durée_retard :INTEGER )

* **Deterioration**(#id_pret => Pret.id_pret , etat_remboursement : {etat_remboursement_en attente ,
  etat_remboursement_fait})

* **Exemplaire**(#id : integer , ressource =>Ressource.code, etat : {etat_neuf , etat_bon , etat_correct , etat_abimé ,
  etat_tres_abimé}, ressource => Ressource.code)


## Partie 3

* **Contributeur**(#id : integer, nom: VARCHAR, prenom: VARCHAR, data_de_naissance : date, nationalité : VARCHAR)


Un héritage par référence a été choisi parce que Ressource est directement réferéncé par 1...* Examplaires. Ne sachant pas si Examplaire référence un Livre, un Film ou une Musique, on décide de garder la table Ressource en tant que classe abstraite.
L'héritage par classe mère est une possibilité intéressante. Elle est mis à l'écart en raison de l'abstraction de la classe mère et des complictions survenants avec les associations Ressource-Contributeur en fonction du type de ressource.  

Il faut ajouter la contrainte que tous les tuples de Ressource sont référencés par un tuple de Livre ou Film ou Musique.
* **Ressource**(#code : VARCHAR, titre: VARCHAR,editeur: VARCHAR, genre: VARCHAR, code_de_classification : VARCHAR)


* **Livre**(#ressource =>Ressource.code, ISBN: VARCHAR, resumé : VARCHAR, lang_doc : VARCHAR )
    * **Auteur**(#contrib =>Contributeur.id, #livre =>Livre.ressource)


* **Film**(#ressource =>Ressource.code, synopsis : VARCHAR, longueur: integer, ) 

    *   **FilmRole** enum('Realisteur', 'Acteur') 
    * **FilmContributeur**(#contrib =>Contributeur.id, #film => Film.ressource, role : FilmRole) avec role not null
  



* **Musique**(#ressource =>Ressource.code, longueur: integer)

    * **MusiqueRole** enum('Compositeur', 'Interprète') 

     * **MusiqueContributeur**(#contrib =>Contributeur.id, #musique => Musique.ressource, role : MusiqueRole) avec role not null

   
