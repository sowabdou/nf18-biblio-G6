drop table if exists Membres;
drop table if exists Adherents;
drop table if exists MembreDuPersonnel;
drop table if exists CompteUtilisateur;
drop table if exists Contributeurs;
drop table if exists Ressources;
drop table if exists Livres_infos;
drop table if exists Films_infos;
drop table if exists Musiques_infos;
drop table if exists Pret;
drop table if exists Retard;
drop table if exists Deterioration;
drop table if exists Exemplaire;
-- # Gestion des utilisateurs
create table Membres
(
    id       int primary key,
    nom      varchar(50)  not null,
    prenom   varchar(25)  not null,
    addresse varchar(255) not null,
    email    varchar(255) not null
);
create table Adherents
(
    id_membre int unique,
    birthday  date,
    phone     varchar(15),
    card      varchar(50) unique,
    foreign key (id_membre) references Membres (id)
);
create table MembreDuPersonnel
(
    id_membre int unique,
    foreign key (id_membre) references Membres (id)
);
create table CompteUtilisateur
(
    id_membre int unique,
    login     varchar(10) unique,
    password  varchar(15) not null,
    foreign key (id_membre) references Membres (id)
);

-- # Partie 3 contenue de la bibliothèque
create table Contributeurs
(
    id             int primary key,
    nom            varchar(50) not null,
    prenom         varchar(25) not null,
    date_naissance date        not null,
    nationalite    char(2)     not null -- Code pays ISO 3166-1 alpha-2
);
-- sous types de contributeurs
create table Auteurs
(
    id_contributeur int,
    code_ressource  varchar(10),
    primary key (id_contributeur, code_ressource),
    foreign key (id_contributeur) references Contributeurs (id) on delete cascade,
    foreign key (code_ressource) references Ressources (code) on delete cascade
);
CREATE TYPE FilmContributeurRole AS ENUM ('acteur', 'realisateur');
create table FilmContributeurs
(
    id_contributeur int,
    code_ressource  varchar(10),
    role            FilmContributeurRole not null,
    primary key (id_contributeur, code_ressource),
    foreign key (id_contributeur) references Contributeurs (id) on delete cascade,
    foreign key (code_ressource) references Ressources (code) on delete cascade
);
CREATE TYPE MusiqueContributeurRole AS ENUM ('compositeur', 'interprete');
create table MusiqueContributeurs
(
    id_contributeur int,
    code_ressource  varchar(10),
    role            MusiqueContributeurRole not null,
    primary key (id_contributeur, code_ressource),
    foreign key (id_contributeur) references Contributeurs (id) on delete cascade,
    foreign key (code_ressource) references Ressources (code) on delete cascade
);

--      Ressources
create table Ressources
(
    code                varchar(10) primary key,
    titre               varchar(255) not null,
    editeur             varchar(255) not null,
    genre               varchar(100) not null,
    code_classification varchar(10)  not null
);

create table Livres_infos
(
    code_ressource varchar(10) primary key,
    isbn           varchar(13) not null,
    resume         varchar(1000),
    langue         char(2)     not null, -- Code langue ISO 639-1
    foreign key (code_ressource) references Ressources (code) on delete cascade
);
create table Films_infos
(
    code_ressource varchar(10) primary key,
    duree          int not null,
    synopsis       varchar(1000),
    foreign key (code_ressource) references Ressources (code) on delete cascade
);

create table Musiques_infos
(
    code_ressource varchar(10) primary key,
    duree          int not null,
    foreign key (code_ressource) references Ressources (code) on delete cascade
);
-- Views pour les infos complémentaires des ressources simplifiant les requêtes
create view Livres as
select *
from Ressources
         inner join Livres_infos on Ressources.code = Livres_infos.code_ressource;
create view Films as
select *
from Ressources
         inner join Films_infos on Ressources.code = Films_infos.code_ressource;
create view Musiques as
select *
from Ressources
         inner join Musiques_infos on Ressources.code = Musiques_infos.code_ressource;

-- # Partie 2 prestations de la bibliothèque
CREATE TYPE etat_type AS ENUM ('en cours','etat_fini');
create table Pret
(
    id_pret    integer primary key,
    exemplaire int       not null,
    adherent   int       not null,
    date_debut date,
    etat       etat_type NOT NULL,
    duree      integer,
    foreign key (exemplaire) references Exemplaire (id),
    foreign key (adherent) references Adherent (id_membre)
);

create table Retard
(
    id_pret      int primary key,
    date_retour  date,
    duree_retard integer,
    foreign key (id_pret) references Pret (id_pret)
);

CREATE TYPE etat AS ENUM ('etat_remboursement_en attente','etat_remboursement_fait');
create table Deterioration
(
    id_pret            int primary key,
    etat_remboursement etat NOT NULL,
    foreign key (id_pret) references Pret (id_pret)
);

CREATE TYPE etat_Exemplaire AS ENUM ('etat_neuf','etat_bon','etat_correct','etat_abime','etat_tres_abime');
create table Exemplaire
(
    id        integer primary key,
    ressource varchar(10),
    etat      etat_Exemplaire NOT NULL,
    foreign key (ressource) references Ressources (code),

);
