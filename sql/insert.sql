INSERT INTO Pret 
VALUES (
    '19-10-2022',
    'en cours',
    '1',
    '167'
);
INSERT INTO Retard 
VALUES (
    '22-10-2022',
    '20',
    '1'
);
INSERT INTO Deterioration 
VALUES (
    '1',
    'etat_remboursement_en attente'
);
INSERT INTO Exemplaire 
VALUES (
    '1',
    'A12E',
    'etat_neuf'
);
-----------Les Miserables------------------------
INSERT INTO Ressources 
VALUES(
    'A12E',
    'Les Miserables',
    'Ldp Jeunesse',
    'fiction',
    '53'
);
INSERT INTO Livres_infos
VALUES(
    'A12E',
    '2010008995',
    'Un classique de literrature francaise',
    'FR'
);

INSERT INTO Contributeurs
VALUES(
    1,
    'Hugo',
    'Victor',
    26-02-1802,
    'France'
);

INSERT INTO Auteurs
VALUES(
    1,
    'A12E'
);

-------------------Marche Turc---------------------------------d

INSERT INTO Ressources 
VALUES(
    'MT88',
    'Marche Turc',
    'Erwan Records',
    'classique',
    '89M'
);
INSERT INTO Musique_infos
VALUES(
    'MT88',
    '400'
);
INSERT INTO Contributeurs
VALUES(
    2,
    'Mozart',
    'Wolfgang Amadeus',
    27-01-1756,
    'Autriche'
);

INSERT INTO MusiqueContributeurs
VALUES(
    2,
    'MT88',
    'compositeur'
);


---------------Titanic---------------------
INSERT INTO Ressources
VALUES(
    'TT09',
    'Titanic',
    'Hollywood',
    'Drame',
    'FT67'
);

INSERT INTO Films_infos
VALUES(
    'TT09',
    200,
    'Un bateau frappe un iceberg'
);

INSERT INTO Contributeurs
VALUES(
    3,
    'Cameron',
    'James',
    16-08-1954,
    'Etats-Unis'
);
INSERT INTO FilmContributeurs 
VALUES (
    3,
    'TT09',
    'realisateur'
);
